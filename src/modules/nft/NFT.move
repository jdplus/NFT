address 0x793f22E6F3A8D4A38374A766F905982f {
module NFT {
    use 0x1::Signer;
    use 0x1::Vector;

    struct NFT has key, store { name: vector<u8> }

    struct UniqIdList has key, store {
        data: vector<vector<u8>>
    }

    public fun initialize(account: &signer) {
        let addr = Signer::address_of(account);
        if (!exists<UniqIdList>(addr)){
            move_to(account, UniqIdList {data: Vector::empty<vector<u8>>()});
        };
    }

    public fun new(_account: &signer, registe_center: address, name: vector<u8>): NFT acquires UniqIdList {
        assert(exists<UniqIdList>(registe_center), 10000);
        let exist = Vector::contains<vector<u8>>(&borrow_global<UniqIdList>(registe_center).data, &name);
        assert(!exist, 10001);
        let id_list = borrow_global_mut<UniqIdList>(registe_center);
        Vector::push_back<vector<u8>>(&mut id_list.data, copy name);
        NFT { name }
    }

    public fun destroy(registe_center: address, nft: NFT) acquires UniqIdList {
        assert(exists<UniqIdList>(registe_center), 10002);
        let NFT { name } = nft;
        let (exist, index) = Vector::index_of<vector<u8>>(&borrow_global<UniqIdList>(registe_center).data, &name);
        assert(!exist, 10003);
        let id_list = borrow_global_mut<UniqIdList>(registe_center);
        Vector::remove<vector<u8>>(&mut id_list.data, index);
    }
}
}
